ARG DOCKER_BASE_IMAGE_PREFIX
ARG DOCKER_BASE_IMAGE_NAMESPACE=pythonpackagesonalpine
ARG DOCKER_BASE_IMAGE_NAME=basic-python-packages-pre-installed-on-alpine
FROM ${DOCKER_BASE_IMAGE_PREFIX}${DOCKER_BASE_IMAGE_NAMESPACE}/${DOCKER_BASE_IMAGE_NAME}:pip-alpine

ARG FIX_ALL_GOTCHAS_SCRIPT_LOCATION
ARG ETC_ENVIRONMENT_LOCATION
ARG CLEANUP_SCRIPT_LOCATION

# Depending on the base image used, we might lack wget/curl/etc to fetch ETC_ENVIRONMENT_LOCATION.
ADD $FIX_ALL_GOTCHAS_SCRIPT_LOCATION .
ADD $CLEANUP_SCRIPT_LOCATION .

RUN set -o allexport \
    && . ./fix_all_gotchas.sh \
    && set +o allexport \
    && apk add --no-cache py3-ruamel.yaml py3-jinja2 py3-click py3-tox py3-setuptools_scm \
    && python -m pip install --no-cache-dir --break-system-packages --no-build-isolation --extra-index-url https://alpine-wheels.github.io/index git+https://github.com/dHannasch/cookiecutter.git@branch-to-run \
    && python -m pip install --no-cache-dir --break-system-packages --no-build-isolation --extra-index-url https://alpine-wheels.github.io/index git+https://github.com/dHannasch/python-cookiepatcher.git@allow-tab-complete-target-directory-name \
    && . ./cleanup.sh

