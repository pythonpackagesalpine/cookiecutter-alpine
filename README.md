This container contains everything you need to run cookiecutters.

To use it, you can manually pull the container and run it.

Of course, the easiest way to use it is via a GitLab-Runner, by placing in your .gitlab-ci.yml:

    include:
    - project: 'shell-bootstrap-scripts/cookiecutter-deployment'
      file: 'cookiecutter_deployment.yaml'
      ref: master

    deploy_cookiecutter_to_child_repo:
      extends: .deploy_cookiecutter_to_child_repo
